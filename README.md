<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.91 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.13 -->
# kivy_file_chooser 0.3.11

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_file_chooser/develop?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_file_chooser)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_file_chooser/release0.3.10?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_file_chooser/-/tree/release0.3.10)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_kivy_file_chooser)](
    https://pypi.org/project/ae-kivy-file-chooser/#history)

>ae namespace module portion kivy_file_chooser: extended kivy file chooser widget.

[![Coverage](https://ae-group.gitlab.io/ae_kivy_file_chooser/coverage.svg)](
    https://ae-group.gitlab.io/ae_kivy_file_chooser/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_kivy_file_chooser/mypy.svg)](
    https://ae-group.gitlab.io/ae_kivy_file_chooser/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_kivy_file_chooser/pylint.svg)](
    https://ae-group.gitlab.io/ae_kivy_file_chooser/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_kivy_file_chooser)](
    https://gitlab.com/ae-group/ae_kivy_file_chooser/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_kivy_file_chooser)](
    https://gitlab.com/ae-group/ae_kivy_file_chooser/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_kivy_file_chooser)](
    https://gitlab.com/ae-group/ae_kivy_file_chooser/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_kivy_file_chooser)](
    https://pypi.org/project/ae-kivy-file-chooser/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_kivy_file_chooser)](
    https://gitlab.com/ae-group/ae_kivy_file_chooser/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_kivy_file_chooser)](
    https://libraries.io/pypi/ae-kivy-file-chooser)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_kivy_file_chooser)](
    https://pypi.org/project/ae-kivy-file-chooser/#files)


## installation


execute the following command to install the
ae.kivy_file_chooser module
in the currently active virtual environment:
 
```shell script
pip install ae-kivy-file-chooser
```

if you want to contribute to this portion then first fork
[the ae_kivy_file_chooser repository at GitLab](
https://gitlab.com/ae-group/ae_kivy_file_chooser "ae.kivy_file_chooser code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_kivy_file_chooser):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_kivy_file_chooser/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.kivy_file_chooser.html
"ae_kivy_file_chooser documentation").
